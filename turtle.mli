
  type t

  val create : (float * float) -> Instruction.t list -> t

  val forward : float -> t -> t
  val backward : float -> t -> t
  val left : float -> t -> t
  val right : float -> t -> t
  val pen_up : t -> t
  val pen_down : t -> t
  val pen_switch : t -> t
  val color : string -> t -> t
  val color_rgb : int -> int -> int -> t -> t

  val display : t -> unit
  val run : t -> unit
