open Instruction


let height = 800
let width = 800
let window_size = (height,width)

let initial_coordinates = 
(
  (float_of_int height) /. 2. 
, 
  (float_of_int width) /. 2.
)

(* initialize Displayer *)
let () = Displayer.init window_size initial_coordinates

let instruction_strings = FileReader.get_instructions_strings_from_file "input.turtle"
let instruction_list = InstructionsParser.get_list_instructions_of_list_strings instruction_strings


(* create a turtle with its instructions *)
let turtle01 = Turtle.create initial_coordinates instruction_list


(* make turtle processing all its instructions *)
let _ = Turtle.run turtle01


(* wait a click before the end of program *)
let event = Graphics.wait_next_event
	      [Graphics.Button_down]
