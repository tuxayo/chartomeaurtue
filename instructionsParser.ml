open Instruction



let rec get_list_list_words_of_list_string string_list words_list_list = 
  match string_list with
  | [] -> List.rev words_list_list
  | first::others -> let subs = Str.split (Str.regexp "[ \t]+") first in
		     get_list_list_words_of_list_string others (subs::words_list_list)


let get_instruction instruction_name parameters_list = 
  match instruction_name with
  | "Forward" -> Forward (float_of_string (List.hd parameters_list))
  | "Backward"-> Backward (float_of_string (List.hd parameters_list))
  | "Left" -> Left (float_of_string (List.hd parameters_list))
  | "Right" -> Right (float_of_string (List.hd parameters_list))
  | "Pen_Up" -> Pen_Up
  | "Pen_Down" -> Pen_Down
  | "Pen_Switch" -> Pen_Switch
  | "Grow" -> Grow (float_of_string (List.hd parameters_list))
  | "Shrink" -> Shrink (float_of_string (List.hd parameters_list))
  | "Color" -> Color (List.hd parameters_list)
  | "Color_RGB" -> let r = int_of_string (List.hd parameters_list) in
		   let g = int_of_string (List.hd (List.tl parameters_list)) in
		   let b = int_of_string (List.hd (List.tl (List.tl parameters_list))) in
		   Color_RGB (r,g,b)
  | "End" -> End
  | "Label" -> Label (List.hd parameters_list)
  | "Jump" -> Jump (List.hd parameters_list)
  | "Repeat" -> Repeat (int_of_string (List.hd parameters_list))
  | _ -> failwith "This string is an unknown instruction"


let get_instruction_of_list_words word_list =
  match word_list with
  | [] -> failwith "Calling the function get_instruction_from_a_word_list with empty list of words is forbidden"
  | name::arguments -> 
     (
       get_instruction 
	 name
	 arguments
     )



let rec get_list_instruction_of_list_list_words list_list_word =
  match list_list_word with
  | [] -> []
  | first_list_words::others_list_words -> 
     (
       get_instruction_of_list_words
	 first_list_words
     )
     ::
       get_list_instruction_of_list_list_words
	 others_list_words



let get_list_instructions_of_list_strings list =
  let string_list_list = 
    get_list_list_words_of_list_string 
      list 
      [] 
  in
  get_list_instruction_of_list_list_words
    string_list_list
