open Vector
open Instruction
open Displayer

type pencil_state =
  | Up
  | Down


type t = {
  pos: Vector.t;
  angle: Vector.t;
  pencil: pencil_state;
  color: (int * int * int);
  size: float;
  initial_instructions: Instruction.t list;
  remaining_instructions: Instruction.t list;
}



let create (initial_x,initial_y) instruction_list =
  {
    pos = Vector.create initial_x initial_y;
    angle = Vector.create 1. 0.;
    pencil = Up;
    color = (0xFF,0xFF,0xFF);
    initial_instructions = instruction_list;
    remaining_instructions = instruction_list;
    size = 1.;
  }



let forward distance turtle =
  let move_vector = (distance *. turtle.size) *: turtle.angle in
  let turtle_moved = {
    turtle with
    pos = turtle.pos +: move_vector
  } in

  match turtle_moved.pencil with (* TODO refactor this sillyness (let in match) *)
  | Down -> let _ = Displayer.draw_line_from_current_pos move_vector in
           turtle_moved
  | Up -> let _ = Displayer.move_current_pos move_vector in
         turtle_moved



let backward distance turtle =
  forward (-.distance) turtle



let left angle turtle =
  { turtle with
    angle = Vector.rotate ~angle:angle turtle.angle
  }



let right angle turtle = 
  left (-.angle) turtle



let pen_up turtle = 
    { turtle with 
      pencil = Up
    } 



let pen_down turtle = 
  { turtle with 
    pencil = Down
  } 



let pen_switch turtle =
  if turtle.pencil = Up then 
    { turtle with 
      pencil = Down
    }
  else
    { turtle with 
      pencil = Up
    }



let grow factor turtle =
  { turtle with 
    size = turtle.size *. factor
  }



let shrink factor turtle =
  { turtle with 
    size = turtle.size /. factor
  }



let end_instr turtle =
  Printf.printf "Program ended \n";
  { turtle with
    remaining_instructions = []
  }



let color svg_color_name turtle =

  let (r,g,b) = Displayer.get_RGB_from_color_name svg_color_name in

  let turtle_modified = 
    { turtle with 
      color = (r,g,b)
    } 
  in
  let _ = Displayer.set_pencil_color_rgb (r,g,b) in
  turtle_modified
  


let color_rgb r g b turtle =
  let turtle_modified = 
    { turtle with 
      color =  (r,g,b)
    } 
  in
  let _ = Displayer.set_pencil_color_rgb (r,g,b) in
  turtle_modified



let update_instructions turtle remaining_instructions =
  { turtle with
    remaining_instructions = remaining_instructions
  }



let prepend_instructions instructions turtle =
  { turtle with
    remaining_instructions = instructions@turtle.remaining_instructions
  }



let jump label turtle =
  (* find instructions between label and the matching end *)
  let next_instructions = Instruction.find_label_block label turtle.initial_instructions
  in
  prepend_instructions next_instructions turtle



(* Does not support label/end blocks inside, that whould be stupid to jump inside
   a loop, user should extract the label/end block *)
(* Does not support repeat 0 *)
let repeat times turtle =
  if times > 1 then
    let next_instructions = Instruction.extract_repeat_iteration times turtle.remaining_instructions
    in
    prepend_instructions next_instructions turtle
  else (* repeat 1 time *)
    Instruction.remove_matching_end turtle.remaining_instructions
  |> update_instructions turtle



let display turtle =

  let str_of_pencil pencil =
    match pencil with
    | Up -> "Up"
    | Down -> "Down"
  in

  let (r,g,b) = turtle.color in

  Printf.printf "DEBUG: New state of turtle:\n\
		 pos   : (%f,%f)\n\
		 angle : %d degrés\n\
		 pencil : %s\n\
		 color : %d %d %d\n\
		 size : %f\n\n"
		(Vector.x turtle.pos)
		(Vector.y turtle.pos)
		(Vector.angle turtle.angle)
		(str_of_pencil turtle.pencil)
		(r)(g)(b)
		(turtle.size)



let process_instruction instruction turtle =
  let turtle_result =
    match instruction with
    | Forward(distance) -> forward distance turtle
    | Backward(distance) -> backward distance turtle
    | Left(angle) -> left angle turtle
    | Right(angle) -> right angle turtle
    | Pen_Up -> pen_up turtle
    | Pen_Down -> pen_down turtle
    | Pen_Switch -> pen_switch turtle
    | Grow(factor) -> grow factor turtle
    | Shrink(factor) -> shrink factor turtle
    | Color(svg_color_name) -> color svg_color_name turtle
    | Color_RGB(r,g,b) -> color_rgb r g b turtle
    | End -> end_instr turtle
    | Label(label_name) -> turtle
    | Jump(label_name) -> jump label_name turtle
    | Repeat(times) -> repeat times turtle
  in
  turtle_result



let rec iterate_instructions turtle =
  match turtle.remaining_instructions with
  | [] -> ()
  | first::others -> iterate_instructions (process_instruction first (update_instructions turtle others))



let rec run turtle =
  match turtle.initial_instructions with
  | [] -> failwith "Error: give me instructions!!"
  | instructions -> iterate_instructions turtle
