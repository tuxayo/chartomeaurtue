type t =
  | Forward of float
  | Backward of float
  | Left of float
  | Right of float
  | Pen_Up
  | Pen_Down
  | Pen_Switch
  | Grow of float
  | Shrink of float
  | Color of string
  | Color_RGB of int * int * int
  | End
  | Label of string
  | Jump of string
  | Repeat of int



let string_of_instruction instruction =
  match instruction with
  | Forward(distance) -> "Forward "^(string_of_float distance)
  | Backward(distance) -> "Backward "^(string_of_float distance)
  | Left(angle) -> "Left "^(string_of_float angle)
  | Right(angle) -> "Right "^(string_of_float angle)
  | Pen_Up -> "Pen_Up"
  | Pen_Down -> "Pen_Down"
  | Pen_Switch -> "Pen_Switch"
  | Grow(factor) -> "Grow "^(string_of_float factor)
  | Shrink(factor) -> "Shrink "^(string_of_float factor)
  | Color(css_name) -> "Color "^css_name
  | Color_RGB(r,g,b) -> "Color_RGB "^(string_of_int r)
                        ^" "^(string_of_int g)
                        ^" "^(string_of_int b)
  | End -> "End"
  | Label(name) -> "Label "^name
  | Jump(label) -> "Jump "^label
  | Repeat(times) -> "Repeat "^(string_of_int times)



let print_instruction instruction =
  Printf.printf "instruction: '%s'\n"
    (string_of_instruction instruction)



let display_instructions instruction_list =
  List.iter (fun instruction -> (print_instruction instruction)) instruction_list



let match_with_label instruction label_name =
  match instruction with
  | Label(name) when name = label_name -> true
  | _ -> false



let rec iter_until_label instructions label =
  match instructions with
  | [] -> failwith "error: label not found in instruction list"
  | first::others when match_with_label first label -> others
  | first::others -> iter_until_label others label



(* blocks can be nested like parentesis, this is why there is a depth *)
let rec find_label_block_end label instructions_to_scan instructions_kept depth =
  match instructions_to_scan with
  | Label(name)::others when name = label
    -> failwith "error: two labels with same name"
  | Label(name)::others
    -> find_label_block_end label others (Label(name)::instructions_kept) (depth+1)
  | Repeat(times)::others
    -> find_label_block_end label others (Repeat(times)::instructions_kept) (depth+1)
  | End::others when depth = 0 -> instructions_kept
  | End::others
    -> find_label_block_end label others (End::instructions_kept) (depth-1)
  | first::others
    -> find_label_block_end label others (first::instructions_kept) depth
  | _ -> failwith ("error: matching end for label "^label^" not found")



let find_label_block label_name instructions =
  let instructions_after_label = iter_until_label instructions label_name in
  find_label_block_end label_name instructions_after_label [] 0
  |> List.rev



(* repeat blocks can be nested, this is why there is a depth *)
let rec find_repeat_block_end instructions_to_scan instructions_kept depth =
  match instructions_to_scan with
  | Label(_)::_ -> failwith "error: No label allowed in repeat blocks, extract it and call it with jump\n, what where you think when doing this!?"
  | Repeat(times)::others
    -> find_repeat_block_end others (Repeat(times)::instructions_kept) (depth+1)
  | End::others when depth = 0 -> instructions_kept
  | End::others
    -> find_repeat_block_end others (End::instructions_kept) (depth-1)
  | first::others
    -> find_repeat_block_end others (first::instructions_kept) depth
  | _ -> failwith "error: matching end for repeat block not found"



let add_repeat_if_necessary block_content times =
  if times > 1 then
    Repeat(times-1)::block_content
  else
    block_content



(* find block and extract one iteration *)
let extract_repeat_iteration times instructions =
  let block_content = find_repeat_block_end instructions [] 0 in
  add_repeat_if_necessary block_content times
  |> List.rev


let rec rec_remove_matching_end instructions_to_scan instructions_kept depth =
  match instructions_to_scan with
  | Repeat(times)::others
    -> rec_remove_matching_end others (Repeat(times)::instructions_kept) (depth+1)
  | End::others when depth = 0 -> (List.rev instructions_kept)@others
  | End::others -> rec_remove_matching_end others (End::instructions_kept) (depth-1)
  | first::others -> rec_remove_matching_end others (first::instructions_kept) depth
  | _ -> failwith "should not happen, it would have failled in find_repeat_block_end"


let remove_matching_end remaining_instructions =
  rec_remove_matching_end remaining_instructions [] 0
