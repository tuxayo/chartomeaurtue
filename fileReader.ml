


let rec get_lines_from_channel channel list =
  try
    let line = 
      input_line channel 
    in
    get_lines_from_channel channel (line::list)

  with End_of_file ->
    let _ = 
      close_in channel 
    in
    List.rev list



let get_lines_from_file file_name =
  get_lines_from_channel (open_in file_name) []

  

let rec get_lists_of_words_from_lines list_lines subchain_list = 
  match list_lines with
  | [] -> List.rev subchain_list
  | first::others -> let subs = Str.split (Str.regexp "[ \t]+") first in
		     get_lists_of_words_from_lines others (subs::subchain_list)



let get_str_of_instruction_name str = 
  let str_lower_case = String.lowercase str in

  if String.compare str_lower_case "forward" == 0 then "Forward"
  else if String.compare str_lower_case "backward" == 0 then "Backward"
  else if String.compare str_lower_case "left" == 0 then "Left"
  else if String.compare str_lower_case "right" == 0 then "Right"
  else if String.compare str_lower_case "penup" == 0 then "Pen_Up"
  else if String.compare str_lower_case "pendown" == 0 then "Pen_Down"
  else if String.compare str_lower_case "penswitch" == 0 then "Pen_Switch"
  else if String.compare str_lower_case "grow" == 0 then "Grow"
  else if String.compare str_lower_case "shrink" == 0 then "Shrink"
  else if String.compare str_lower_case "color" == 0 then "Color"
  else if String.compare str_lower_case "color_rgb" == 0 then "Color_RGB"
  else if String.compare str_lower_case "end" == 0 then "End"
  else if String.compare str_lower_case "backward" == 0 then "Backward"
  else if String.compare str_lower_case "repeat" == 0 then "Repeat"
  else if String.compare str_lower_case "jump" == 0 then "Jump"
  else "Label " ^ str



let get_str_of_instruction list =
  match list with
  | [] -> ""
  | first::others -> try
		       let _ = Str.search_backward (Str.regexp_string "--") first 2 in
		       ""
		     with Not_found ->
		       String.concat " " ((get_str_of_instruction_name first)::others)



let rec get_str_list_of_instructions list_list =
  match list_list with
  | [] -> []
  | first::others -> let str = get_str_of_instruction first in
		     if (str <> "") then 
			 str::get_str_list_of_instructions others
		     else
		       get_str_list_of_instructions others
						      


let get_instructions_strings_from_file file_name =
  let lines_list = 
    get_lines_from_file file_name 
  in
  let lines_words_list = 
    get_lists_of_words_from_lines lines_list [] 
  in
  get_str_list_of_instructions lines_words_list
