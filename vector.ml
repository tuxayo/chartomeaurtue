 
  type t = float * float



  let create x y =
    (x, y)



  let zero = (0., 0.)



  let pi = acos (-1.)



  let x (x, _) = x



  let y (_, y) = y



  let add (x1,y1) (x2,y2) = 
    (x1+.x2, y1+.y2)

  let (+:) = add


  
  let sub (x1,y1) (x2,y2) =
    (x1-.x2, y1-.y2)

  let (-:) = sub



  let mul scalar (x,y) =
    (x*.scalar, y*.scalar)
 
  let ( *: ) = mul



  let degrees_to_rad degrees =
    degrees *. pi/. 180.



  let rad_to_degrees radians =
    radians *. 180. /. pi



  let rotate ~angle (x,y) =
    let angle_rad = degrees_to_rad angle in
    let x_result = (cos angle_rad) *. x +. (-.sin angle_rad) *. y in
    let y_result = (sin angle_rad) *. x +. (cos angle_rad) *. y in
    (x_result, y_result)



  let angle (x,y) =
    let angle_rad = atan2 y x in
    let angle_degrees = rad_to_degrees angle_rad in
    int_of_float angle_degrees
