
  type t
  val create : float -> float -> t
  val zero : t
  val x : t -> float (* first projection *)
  val y : t -> float (* second projection *)
  val (+:) : t -> t -> t
  val (-:) : t -> t -> t
  val ( *: ) : float -> t -> t (* multiply both coordinates by a scalar *)
  val rotate : angle:float -> t -> t (* in degree *)
  val angle : t -> int (* in degree *)
